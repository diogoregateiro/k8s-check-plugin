VERSION ?= 0.9.0
PACKAGE=releases/kubectl-check-v$(VERSION).tar.gz

requirements:
	sudo apt install shellcheck=0.8.0-2 jq curl

lint:
	shellcheck --norc --enable=all kubectl-check

package: lint
	@mkdir -p releases
	@tar czf $(PACKAGE) manifests/* LICENSE kubectl-check
	@sed "s/<HASH>/`sha256sum ${PACKAGE} | cut -d ' ' -f 1`/" check.yaml.tp > check.yaml
	@sed -i "s/<VERSION>/$(VERSION)/" check.yaml
	@scp -q releases/kubectl-check-v$(VERSION).tar.gz ovh:/var/www/html/krew/
	@mv check.yaml ./krew-index/plugins/check.yaml

local-install: package
	kubectl krew install --manifest=./krew-index/plugins/check.yaml --archive=$(PACKAGE)

index-install:
	kubectl krew index add regateiro https://gitlab.com/diogoregateiro/krew-index.git

index-uninstall:
	kubectl krew index remove regateiro

install:
	kubectl krew install regateiro/check

upgrade:
	kubectl krew upgrade

uninstall:
	kubectl krew uninstall check

reinstall: uninstall install

local-reinstall: uninstall local-install
